import time

import pygame

from main.controller.controller import Controller
from main.model.game_state import GameState
from main.model.bodies import Body, Player
from main.model.engine import update_engine
from main.utils.vector_utils import *
from main.view.palette import *
from main.view.view import View


def run(view: View, controller: Controller, state: GameState):
    while state.running:
        start_time = time.clock()

        controller.interpret_events()

        time_one = time.clock()

        state.move_count += state.time_control.value
        state.move_count = max(state.move_count, 0)

        update_engine(state)

        time_two = time.clock()

        view.render(state)

        end_time = time.clock()

        total_time = end_time - start_time
        state.monitoring["FPS"] = str(round(total_time ** -1, 1))
        state.monitoring["Load"] = \
            "Events: %s%% Engine: %s%% Render: %s%% " % ((
                str(round((time_one - start_time) * 100 / total_time, 1)).ljust(4, "0"),
                str(round((time_two - time_one) * 100 / total_time, 1)).ljust(4, "0"),
                str(round((end_time - time_two) * 100 / total_time, 1)).ljust(4, "0"),
            ))


def create_player(
        state,
        position=(100, 100),
        velocity=(0, 0),
        size=10,
        density=1,
        colour=FADED_GREEN,
        name="Green"
) -> Player:
    player = Player(position, velocity, size, density, colour, name)
    state.physical_state.add_body(player)
    state.physical_state.register_player(player)
    return player


if __name__ == "__main__":

    pygame.init()

    view = View()

    state = GameState()

    controller = Controller(state)

    create_player(
        state,
        position=divide_vector((view.info.current_w, view.info.current_h), 3),
        velocity=[0, 0],
        size=50,
        colour=FADED_GREEN,
        name="Green"
    )

    # create_player(
    #     engine,
    #     position=divide_vector((view.info.current_w, view.info.current_h), 5),
    #     velocity=[0, 0],
    #     size=50,
    #     colour=FADED_RED,
    #     name="Red"
    # )
    # self.create_player(position=divide_vector(self.size, 2), velocity=[0, 0], size=10, colour=FADED_RED, name="Red")
    # self.engine.bodies.append(player_two)
    # self.gui.register_player(player_two)
    # for z in range(1, 10):
    #     from random import randint
    #     self.engine.bodies.append(
    #         Body(
    #             position=[randint(0, 12000), randint(0, 12000)],
    #             velocity=[randint(-10, 10), randint(-10, 10)],
    #             size=randint(1, 500),
    #             colour=pygame.Color(45, 32, 22),
    #             density=randint(20, 30)
    #         )
    #     )
    state.physical_state.add_body(
        Body(
            position=[1000, 500],
            velocity=[0, 0],
            size=300,
            colour=pygame.Color(45, 32, 22),
            density=50,
            health=400
        )
    )

    run(view, controller, state)

