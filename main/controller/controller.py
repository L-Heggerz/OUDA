import pygame
from pygame.event import Event

from main.model.bodies import LeftMouseActions, Body, FADED_RED, Player, DARK_GREY
from main.model.game_state import Time, GameState
from main.utils.vector_utils import subtract_vectors, add_vectors, multiply_vector, normalize_vector


def shutdown_nicely():
    pygame.quit()


class Controller:
    def __init__(self, state: GameState):
        self.state: GameState = state
        self.r_mouse_down: bool = False
        self.l_mouse_down: bool = False

    def interpret_events(self):
        for event in pygame.event.get():
            try:
                player = self.state.get_current_player()
                if event.type == pygame.QUIT:
                    self.state.running = False

                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.state.running = False
                    elif event.key == pygame.K_SPACE:
                        self.state.toggle_time()
                    elif event.key == pygame.K_m:
                        self.state.toggle_monitoring()
                    elif event.key == pygame.K_LEFT:
                        self.state.prev_player()
                    elif event.key == pygame.K_RIGHT:
                        self.state.next_player()
                    elif event.key == pygame.K_BACKQUOTE:
                        self.state.toggle_ui()
                    elif event.key == pygame.K_1:
                        self.state.toggle_action(LeftMouseActions.MISSILE)
                    elif event.key == pygame.K_2:
                        self.state.toggle_action(LeftMouseActions.MINE)
                    elif event.key == pygame.K_LCTRL:
                        if player.l_mouse_action == LeftMouseActions.MISSILE:
                            self.fire_missile(player)
                        elif player.l_mouse_action == LeftMouseActions.MINE:
                            self.drop_mine(player)
                    elif event.key == pygame.K_PERIOD:
                        self.state.time_control = Time.FAST_FORWARD
                    elif event.key == pygame.K_COMMA:
                        self.state.time_control = Time.REWIND

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    pygame.mouse.get_rel()
                    if event.button == pygame.BUTTON_RIGHT:
                        self.r_mouse_down = True
                    elif event.button == pygame.BUTTON_LEFT:
                        self.l_mouse_down = True
                        if player.l_mouse_action == LeftMouseActions.MISSILE:
                            player.update_target(self.remove_offset(pygame.mouse.get_pos()))
                        elif player.l_mouse_action == LeftMouseActions.JET:
                            player.update_boost(pygame.mouse.get_pos())
                    elif event.button == pygame.BUTTON_WHEELDOWN:
                        if self.state.move_count > 0:
                            self.state.move_count -= 1
                    elif event.button == pygame.BUTTON_WHEELUP:
                        self.state.move_count += 1

                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == pygame.BUTTON_RIGHT:
                        self.r_mouse_down = False
                    elif event.button == pygame.BUTTON_LEFT:
                        self.l_mouse_down = False
                        player.boost(self.state.offset)

                elif event.type == pygame.MOUSEMOTION:
                    if self.r_mouse_down:
                        self.state.update_offset(pygame.mouse.get_rel())
                    elif self.l_mouse_down:
                        if player.l_mouse_action == LeftMouseActions.MISSILE:
                            player.update_target(self.remove_offset(pygame.mouse.get_pos()))
                        elif player.l_mouse_action == LeftMouseActions.JET:
                            player.update_boost(pygame.mouse.get_pos())

            except Exception as e:
                print("ERROR")
                print("Key: %s" % event)
                print("Exception: %s" % e)

    def remove_offset(self, position):
        return subtract_vectors(position, self.state.offset)

    def drop_mine(self, player):
        self.state.physical_state.add_body(
            Body(
                position=add_vectors(
                    player.position,
                    multiply_vector(normalize_vector(player.velocity), -1 * (player.size + 6))
                ),
                velocity=multiply_vector(normalize_vector(player.velocity), -1),
                size=5,
                density=1,
                colour=FADED_RED,
                name="mine",
                drag_coefficient=0.9,
                flamibility=10.0
            )
        )

    def fire_missile(self, player: Player):
        if player.target is not None:
            self.state.physical_state.add_body(
                Body(
                    position=add_vectors(player.position, multiply_vector(player.aim_vector(), 2 * player.size)),
                    velocity=add_vectors(multiply_vector(player.aim_vector(), 3), player.velocity),
                    size=2,
                    density=1,
                    colour=DARK_GREY,
                    name="missile",
                    drag_coefficient=1.01,
                    flamibility=10.0,
                    has_booster=True
                )
            )