from enum import Enum
from typing import List

from main.view.palette import *
from main.utils.vector_utils import *


class Body:
    def __init__(
        self,
        position: tuple = (float(0), float(0)),
        velocity: tuple = (float(0), float(0)),
        size: float = 10,
        density: float = 1,
        colour: Color = DARK_GREY,
        name: str = "UFO",
        drag_coefficient: float = 1.0,
        flamibility: float = 0.5,
        has_booster: bool = False,
        health: int = 100,
        bounciness: float = 0.5
    ):
        self.position = self.x, self.y = position
        self.velocity = self.x_velocity, self.y_velocity = velocity
        self.size = size
        self.colour = colour
        self.density = density
        self.mass = size * density
        self.name = name
        self.has_booster = has_booster
        self.drag_coefficient = drag_coefficient
        self.flamibility = flamibility
        self.health = health
        self.bounciness = bounciness

    def screen_position(self, offset):
        return int_vector(add_vectors(offset, self.position))

    def step_forward(self):
        self.velocity = self.x_velocity, self.y_velocity = multiply_vector(self.velocity, self.drag_coefficient)
        self.position = self.x, self.y = add_vectors(self.position, self.velocity)

    def step_backward(self):
        self.velocity = self.x_velocity, self.y_velocity = multiply_vector(self.velocity, self.drag_coefficient ** -1)
        self.position = self.x, self.y = add_vectors(self.position, multiply_vector(self.velocity, -1))

    def update_velocity(self, vector):
        self.velocity = self.x_velocity, self.y_velocity = add_vectors(self.velocity, vector)


class LeftMouseActions(Enum):
    JET = 1,
    MISSILE = 2,
    MINE = 3,


class Player(Body):
    def __init__(
        self,
        position: tuple = (0, 0),
        velocity: tuple = (0, 0),
        size: float = 10,
        density: float = 1,
        colour: Color = DARK_GREY,
        name: str = "UFO",
        drag_coefficient: float = 1.0,
        flamibility: float = 1.0,
        l_mouse_action: LeftMouseActions = LeftMouseActions.JET,
        health: int = 100,
        bounciness: float = 0.5
    ):
        super().__init__(
            position,
            velocity,
            size,
            density,
            colour,
            name,
            drag_coefficient,
            flamibility,
            True,
            health,
            bounciness
        )
        self.boost_location = None
        self.target = None
        self.l_mouse_action = l_mouse_action

    def update_boost(self, position):
        self.boost_location = position

    def update_target(self, position):
        self.target = position

    def boost(self, offset):
        if self.boost_location is None:
            return
        pointer_x, pointer_y = self.boost_location
        screen_x, screen_y = self.screen_position(offset)

        vector_x = int((screen_x - pointer_x))
        vector_y = int((screen_y - pointer_y))

        vector_scaler = (vector_x ** 2 + vector_y ** 2) ** -0.5

        self.update_velocity((vector_x * vector_scaler, vector_y * vector_scaler))
        self.boost_location = None

    def aim_vector(self) -> List:
        return normalize_vector(make_vector(self.target, self.position))
