import time

from main.model.game_state import GameState
from main.model.bodies import Body, Player
from main.model.physical_state import PhysicalState
from main.utils.vector_utils import *
from main.view.palette import *

MAX_BODY_COUNT = 200


def update_engine(state: GameState):
    state.monitoring["Bodycount"] = len(state.physical_state.bodies)
    state.monitoring["Move Count"] = \
        "engine - %s game_state %s" % (state.physical_state.move_count, state.move_count)
    while state.physical_state.move_count < state.move_count:
        step_forward(state)
    while state.physical_state.move_count > state.move_count:
        step_backward(state)


def step_forward(state: GameState):
    one = time.clock()
    apply_gravity(state.physical_state)
    two = time.clock()
    do_collisions(detect_collisions(state.physical_state), state.physical_state)
    three = time.clock()
    for body in state.physical_state.bodies:
        body.step_forward()
    four = time.clock()
    try:
        state.monitoring["Engine Load"] = \
            "Gravity: %s%% Collisions: %s%% Movement: %s%%" % ((
                str(round(100 * (two - one) / (four - one), 2)).ljust(4, "0"),
                str(round(100 * (three - two) / (four - one), 2)).ljust(4, "0"),
                str(round(100 * (four - three) / (four - one), 2)).ljust(4, "0")
            )
            )
    except ZeroDivisionError:
        print("hmmm...")
    state.physical_state.move_count += 1


def step_backward(state: GameState):
    one = time.clock()
    reverse_body_changes(state.physical_state)
    undo_collisions(detect_collisions())
    two = time.clock()
    for body in state.physical_state.bodies:
        body.step_backward()
    three = time.clock()
    apply_gravity(-1)
    four = time.clock()
    try:
        state.monitoring["Engine Load"] = \
            "Gravity: %s%% Collisions: %s%% Movement: %s%%" % ((
                str(round(100 * (four - three) / (four - one), 2)).ljust(4, "0"),
                str(round(100 * (two - one) / (four - one), 2)).ljust(4, "0"),
                str(round(100 * (three - two) / (four - one), 2)).ljust(4, "0")
            )
            )
    except ZeroDivisionError:
        print("hmmm")
    state.physical_state.move_count -= 1


def reverse_body_changes(p_state: PhysicalState):
    if p_state.move_count in p_state.body_creations.keys():
        p_state.bodies = [c_body for c_body in p_state.bodies if c_body not in p_state.body_creations[p_state.move_count]]
        p_state.body_creations.pop(p_state.move_count)
    if p_state.move_count in p_state.body_destructions.keys():
        for d_body in p_state.body_destructions[p_state.move_count]:
            p_state.bodies.append(d_body)
            if isinstance(d_body, Player):
                d_body: Player
                p_state.register_player(d_body)
        p_state.body_destructions.pop(p_state.move_count)
    if p_state.move_count in p_state.player_deaths.keys():
        for ship in p_state.player_deaths[p_state.move_count]:
            p_state.register_player(ship)
        p_state.player_deaths.pop(p_state.move_count)
    if p_state.move_count in p_state.explosions.keys():
        p_state.explosions.pop(p_state.move_count)


def apply_gravity(p_state: PhysicalState, constant=1):
    for attractor in p_state.bodies:
        for attractee in p_state.bodies:
            if attractee.position != attractor.position:
                attractee.update_velocity(multiply_vector(calculate_gravity_vector(attractor, attractee), constant))


def detect_collisions(p_state: PhysicalState):
    collisions = []
    for body_one in p_state.bodies:
        for body_two in p_state.bodies:
            if body_one != body_two:
                if distance(body_one.position, body_two.position) < body_one.size + body_two.size:
                    if (body_two, body_one) not in collisions:
                        collisions.append((body_one, body_two))
    return collisions


def blow_up(body: Body, p_state: PhysicalState):
    if body not in p_state.bodies:
        return
    if isinstance(body, Player):
        p_state.add_player_death(body)
    p_state.delete_body(body)
    create_debris(body, p_state)
    # self.explosions.insert(0, (body.position, body.size * body.flamibility, self.move_count))
    p_state.explosions[p_state.move_count] = (body.position, body.size * body.flamibility)


def create_debris(body, p_state: PhysicalState):
    d = 0
    if body.size > 1:
        while d < int(body.size):
            size = randint(1, min(int(body.size / 3) + 1, int(body.size - d)))
            p_state.add_body(
                Body(
                    position=add_vectors(body.position,
                                         (randint(-body.size, body.size), randint(-body.size, body.size))),
                    velocity=add_vectors(body.velocity, (randint(-5, 5), randint(-5, 5))),
                    colour=body.colour,
                    size=size,
                    density=body.density,
                    health=1
                )
            )
            d += 1


# TODO there is WAY more debris than their used to be. Figure it out
def do_collisions(collisions, p_state: PhysicalState):
    for body_one, body_two in collisions:
        body_one.health -= body_two.flamibility * body_two.size
        body_two.health -= body_one.flamibility * body_one.size
        if body_one.health < 0:
            blow_up(body_one, p_state)
        if body_two.health < 0:
            blow_up(body_two, p_state)
        # TODO elastic collisions
        # self.blow_up(lower_mass(body_one, body_two))


def undo_collisions(collisions):
    for body_one, body_two in collisions:
        body_one.health += body_two.flamibility * body_two.size
        body_two.health += body_one.flamibility * body_one.size


def calculate_gravity_vector(attractor: Body, attractee: Body):
    force = min((attractor.mass * attractee.mass) / (distance(attractor.position, attractee.position) ** 2), 100)
    acceleration = force / attractee.mass
    return multiply_vector(normalize_vector(make_vector(attractor.position, attractee.position)), acceleration)