from enum import Enum
from typing import List

from main.model.bodies import Player
from main.model.physical_state import PhysicalState
from main.utils.vector_utils import add_vectors
from main.model.bodies import LeftMouseActions


class Time(Enum):
    FAST_FORWARD = 5
    PLAY = 1
    PAUSED = 0
    REWIND = -5


class GameState:
    def __init__(self):
        self.time_control: Time = Time.PAUSED
        self._current_player = 0
        self.monitoring: dict = {}
        self.monitoring_render: dict = {}
        self.show_monitoring = True
        self._mon_count = 0
        self.show_ui = True
        self.offset: List = [0, 0]
        self.running = True
        self.step = 0
        self.move_set: List[{}] = []
        self.render_count = 0
        self.move_count = 0
        self.physical_state = PhysicalState()

    def toggle_ui(self):
        self.show_ui = not self.show_ui

    def toggle_monitoring(self):
        self.show_monitoring = not self.show_monitoring

    def toggle_time(self):
        if self.time_control == Time.PAUSED:
            self.time_control = Time.PLAY
        else:
            self.time_control = Time.PAUSED

    def toggle_action(self, action: LeftMouseActions):
        player = self.get_current_player()
        if player.l_mouse_action == action:
            player.l_mouse_action = LeftMouseActions.JET
        else:
            player.l_mouse_action = action

    def next_player(self):
        self._current_player += 1

    def prev_player(self):
        self._current_player -= 1

    def get_current_player(self) -> Player:
        if len(self.physical_state.players) == 0:
            return None
        return self.physical_state.players[self._current_player % len(self.physical_state.players)]

    def update_offset(self, update):
        self.offset = add_vectors(self.offset, update)
