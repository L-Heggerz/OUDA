from typing import List, Tuple

from main.model.bodies import Body, Player

MAX_BODY_COUNT = 200


class PhysicalState:
    def __init__(self):
        self.bodies: List[Body] = []
        self.explosions: dict[int, Tuple[int]] = dict()
        self.body_creations: dict[int, List[Body]] = dict()
        self.body_destructions: dict[int, List[Body]] = dict()
        self.player_deaths: dict[int, List[Player]] = dict()
        self.players: List[Player] = []
        self.move_count = 0

    def register_player(self, player: Player):
        self.players.append(player)

    def deregister_player(self, player: Player):
        self.players.remove(player)

    def add_body(self, body: Body):
        if len(self.bodies) < MAX_BODY_COUNT:
            self.bodies.append(body)
            if self.move_count in self.body_creations.keys():
                self.body_creations[self.move_count].append(body)
            else:
                self.body_creations[self.move_count] = [body]

    def delete_body(self, body):
        self.bodies.remove(body)
        if self.move_count in self.body_creations.keys():
            self.body_destructions[self.move_count].append(body)
        else:
            self.body_destructions[self.move_count] = [body]

    def add_player_death(self, player):
        self.deregister_player(player)
        if self.move_count in self.player_deaths.keys():
            self.player_deaths[self.move_count].append(player)
        else:
            self.player_deaths[self.move_count] = [player]