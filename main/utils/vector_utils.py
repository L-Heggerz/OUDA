def distance(point_one, point_two=(0, 0)):
    x1, y1 = point_one
    x2, y2 = point_two
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5


def normalize_vector(vector):
    x, y = vector
    return x * ((x ** 2 + y ** 2) ** -0.5), y * ((x ** 2 + y ** 2) ** -0.5)


def make_vector(point_one, point_two):
    x1, y1 = point_one
    x2, y2 = point_two
    return (x1 - x2), (y1 - y2)


def add_vectors(*args):
    x, y = 0, 0
    for x2, y2 in args:
        x += x2
        y += y2
    return x, y


def subtract_vectors(vector_one, vector_two):
    return make_vector(vector_one, vector_two)


def multiply_vector(vec, mul):
    print(mul)
    x, y = vec
    return (x * mul), (y * mul)


def divide_vector(vec, div):
    x, y = vec
    return x / div, y / div


def int_vector(vec):
    x, y = vec
    return int(x), int(y)
