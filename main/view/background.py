import math
from typing import List

import pygame
from pygame.rect import Rect
from pygame import Surface

from main.view.palette import *

MIN_PLANET_R = 100
MAX_PLANET_R = 600

MAX_STAR_COLOUR_VARIATION = 50


def random(options: list):
    return options[randint(0, len(options) - 1)]


def generate_planets(planet_count, surface):
    max_x, max_y = surface.get_size()
    for star_x in range(0, planet_count):
        radius = randint(MIN_PLANET_R, MAX_PLANET_R)
        planet_pos = planet_x, planet_y = (randint(1, max_x), randint(1, max_y))

        has_ring = randint(1, 10) == 1

        if has_ring:
            pygame.draw.ellipse(
                surface,
                LIGHT_BROWN,
                Rect(
                    planet_x - radius * 1.3,
                    planet_y - radius * 0.3,
                    radius * 2.6,
                    radius * 0.6
                )
            )
            pygame.draw.ellipse(
                surface,
                BLACK,
                Rect(
                    planet_x - radius * 1.05,
                    planet_y - radius * 0.05,
                    radius * 2.1,
                    radius * 0.1
                )
            )

        pygame.draw.circle(surface, BLACK, planet_pos, int(radius * 1.01))
        pygame.draw.circle(surface, random(PLANET_COLOURS), planet_pos, radius)

        if has_ring:
            pygame.draw.arc(
                surface,
                LIGHT_BROWN,
                Rect(
                    planet_x - radius * 1.3,
                    planet_y - radius * 0.3,
                    radius * 2.6,
                    radius * 0.6
                ),
                math.pi,
                math.pi * 2,
                int(radius * 0.25)
            )
            pygame.draw.arc(
                surface,
                BLACK,
                Rect(
                    planet_x - radius * 1.3,
                    planet_y - radius * 0.3,
                    radius * 2.61,
                    radius * 0.61
                ),
                math.pi * 0.78,
                math.pi * 2.22,
                int(radius * 0.01)
            )


def generate_stars(star_count: int, surface: Surface):
    max_x, max_y = surface.get_size()
    for x in range(0, star_count):
        length = randint(4, 10) * 2
        width = randint(2, 5) * 2
        star_x = randint(1, max_x)
        star_y = randint(1, max_y)
        star_colour = pygame.Color(
            255 - randint(1, MAX_STAR_COLOUR_VARIATION),
            255 - randint(1, MAX_STAR_COLOUR_VARIATION),
            255 - randint(1, MAX_STAR_COLOUR_VARIATION)
        )

        pygame.draw.ellipse(surface, star_colour, Rect(star_x - length / 2, star_y - width / 2, length, width))
        pygame.draw.ellipse(surface, star_colour, Rect(star_x - width / 2, star_y - length / 2, width, length))


class Background:
    def __init__(self, size: List, star_count: int = 0, planet_count: int = 0):
        surface = pygame.Surface(size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        surface.fill(BLACK)
        generate_stars(star_count, surface)
        generate_planets(planet_count, surface)
        self.background: Surface = surface.convert()

    def draw(self, surface: Surface, offset: list):
        x_offset, y_offset = offset
        x_size, y_size = self.background.get_size()
        surface.blit(self.background, ((x_offset - x_size)/2, (y_offset - y_size/2)/2))
