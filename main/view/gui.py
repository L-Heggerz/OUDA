import pygame
from pygame.rect import Rect
from pygame.surface import Surface

from main.model.game_state import GameState, Time
from main.view.palette import *
from main.utils.vector_utils import *
from main.view.sprites import draw_player


class GUI:

    def __init__(self, screen: Surface):
        self._screen = screen
        self._font = pygame.font.SysFont(pygame.font.get_default_font(), 18)
        self.width, self.height = screen.get_size()

    def render(self, game_state: GameState):
        if game_state.show_ui:
            if len(game_state.physical_state.players) != 0:
                self.render_vectors(game_state)
                if game_state.get_current_player().target is not None:
                    self.render_crosshair(game_state)
            self.render_toolbar(game_state)
        if game_state.time_control == Time.PAUSED:
            self.render_pause()
        if game_state.show_monitoring:
            self.render_monitoring(game_state)

    def render_monitoring(self, game_state):
        if game_state.render_count % 10 == 0:
            game_state.monitoring_render = game_state.monitoring.copy()
        pixels = 1
        for key, value in game_state.monitoring_render.items():
            self._screen.blit(self._font.render("%s: %s" % (key, value), 1, WHITE), (1, pixels))
            pixels += 18

    def render_pause(self):
        pygame.draw.rect(
            self._screen,
            WHITE,
            Rect(
                self._screen.get_width() * 0.9675,
                self._screen.get_height() * 0.025,
                self._screen.get_width() * 0.005,
                self._screen.get_height() * 0.025
            )
        )
        pygame.draw.rect(
            self._screen,
            WHITE,
            Rect(
                self._screen.get_width() * 0.975,
                self._screen.get_height() * 0.025,
                self._screen.get_width() * 0.005,
                self._screen.get_height() * 0.025
            )
        )

    def render_toolbar(self, game_state):
        pygame.draw.rect(
            self._screen,
            DARK_GREY,
            Rect(
                0,
                self.height * 0.9,
                self.width,
                self.height * 0.1
            )
        )
        if len(game_state.physical_state.players) == 0:
            pygame.draw.rect(self._screen, BLACK, Rect(self.width * 0.01, self.height * 0.92, int(self.height * 0.07), int(self.height * 0.07)))
            pygame.draw.line(self._screen, RED, (self.width * 0.015, self.height * 0.93), (self.width * 0.045, self.height * 0.98), 5)
            pygame.draw.line(self._screen, RED, (self.width * 0.045, self.height * 0.93), (self.width * 0.015, self.height * 0.98), 5)
        else:
            player_name = self._font.render(game_state.get_current_player().name.center(int(self.width / 110)), 1, WHITE)
            self._screen.blit(player_name, (self.width * 0.01, self.height * 0.905))
            self._screen.blit(
                pygame.transform.scale(
                    draw_player(game_state.get_current_player()),
                    (int(self.height * 0.07), int(self.height * 0.07))
                ),
                (self.width * 0.01, self.height * 0.92)
            )
        pygame.draw.rect(
            self._screen,
            BLACK,
            Rect(
                self.width * 0.055,
                self.height * 0.91,
                self.width * 0.15,
                self.height * 0.08
            )
        )

    def render_vectors(self, game_state):
        current_player = game_state.get_current_player()
        if current_player.boost_location is not None:
            pygame.draw.aaline(
                self._screen,
                GREEN,
                current_player.screen_position(game_state.offset),
                current_player.boost_location
            )
        for player in game_state.physical_state.players:
            screen_x, screen_y = player.screen_position(game_state.offset)
            pygame.draw.aaline(
                self._screen,
                RED,
                player.screen_position(game_state.offset),
                (screen_x + player.x_velocity * 10, screen_y + player.y_velocity * 10)
            )

    def render_crosshair(self, game_state):
        target_x, target_y = add_vectors(game_state.get_current_player().target, game_state.offset)
        pygame.draw.aaline(self._screen, RED, (target_x - 5, target_y), (target_x + 5, target_y))
        pygame.draw.aaline(self._screen, RED, (target_x, target_y - 5), (target_x, target_y + 5))

        if game_state.get_current_player() is not None:
            game_state.monitoring["Action"] = game_state.get_current_player().l_mouse_action
            if game_state.get_current_player().target is not None:
                game_state.monitoring["Target"] = game_state.get_current_player().target
    #
    # def render_missile_option(self, selected_option):
    #     pass
    #
    # def render_jet_option(self, selected_option):
    #     pass
    #
    # def render_mine_option(self, selected_option):
    #     pass
    #
    # def render_l_mouse_options(self, game_state):
    #     selected_option = game_state.get_current_player().l_mouse_option
    #     render_jet_option(selected_option)
    #     render_missile_option(selected_option)
    #     render_mine_option(selected_option)
