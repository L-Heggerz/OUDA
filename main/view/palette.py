from random import randint

from pygame import Color

WHITE = Color(255, 255, 255)
BLACK = Color(0, 0, 0)
RED = Color(255, 0, 0)
FADED_RED = Color(75, 40, 40)
GREEN = Color(0, 255, 0)
FADED_GREEN = Color(40, 75, 40)
BLUE = Color(0, 0, 255)
FADED_BLUE = Color(40, 40, 75)
ORANGE = Color(255, 150, 100)
DARK_GREY = Color(100, 100, 100)
LIGHT_GREY = Color(200, 200, 200)
LIGHT_BROWN = Color(95, 75, 40),

PLANET_COLOURS = [
    Color(40, 60, 90),
    Color(20, 90, 40),
    Color(110, 50, 0),
    Color(115, 85, 50),
    Color(100, 100, 100)
]


def random_orange() -> Color:
    return Color(155 + randint(1, 100), 70 + randint(1, 80), 20 + randint(1, 80))

