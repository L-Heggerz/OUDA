from random import randint

import pygame
from pygame.surface import Surface

from main.model.bodies import Body
from main.utils.vector_utils import divide_vector, multiply_vector, normalize_vector
from main.view.palette import LIGHT_GREY, DARK_GREY, random_orange


def draw_body(body: Body):
    return draw_body_with_surface(body, Surface((body.size * 4, body.size * 4)))


def draw_body_with_surface(body: Body, surface):
    if body.x_velocity != 0 and body.y_velocity != 0 and body.has_booster:
        draw_booster(body, surface)
    x, y = divide_vector(surface.get_size(), 2)
    pygame.draw.circle(surface, LIGHT_GREY, (int(x), int(y)), body.size + 1)
    pygame.draw.circle(surface, body.colour, (int(x), int(y)), body.size)
    return surface


def draw_player(body) -> Surface:
    surface = Surface((body.size * 4, body.size * 4))
    if body.target is not None:
        draw_cannon(body, surface)
    return draw_body_with_surface(body, surface)


def draw_cannon(body, surface):
    cent_x, cent_y = divide_vector(surface.get_size(), 2)
    cannon_x, cannon_y = multiply_vector(body.aim_vector(), 2 * body.size)
    pygame.draw.line(surface, DARK_GREY, (int(cent_x), int(cent_y)), (int(cent_x + cannon_x), int(cent_y + cannon_y)), int(0.4 * body.size))


def draw_booster(body, surface):
    orange = random_orange()
    cent_x, cent_y = divide_vector(surface.get_size(), 2)

    boost_x, boost_y = multiply_vector(normalize_vector(body.velocity), ((13 + randint(0, 4)) * (body.size / 10)))
    pygame.draw.line(
        surface,
        orange,
        (int(cent_x), int(cent_y)),
        (int(cent_x - boost_x), int(cent_y - boost_y)),
        int(3 + randint(0, 2) * (body.size / 10))
    )

    boost_x, boost_y = multiply_vector(normalize_vector(body.velocity), (11 + randint(0, 4) * (body.size / 10)))
    pygame.draw.line(
        surface,
        orange,
        (int(cent_x), int(cent_y)),
        (int(cent_x - boost_x), int(cent_y - boost_y))
    )