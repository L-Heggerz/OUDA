from random import randint

import pygame
from pygame.surface import Surface

from main.model.bodies import Player
from main.model.game_state import GameState
from main.utils.vector_utils import *
from main.view.background import Background
from main.view.gui import GUI
from main.view.palette import random_orange, BLACK
from main.view.sprites import draw_body, draw_player


class View:
    def __init__(self):
        self.info = pygame.display.Info()
        pygame.display.set_mode(
            (self.info.current_w, self.info.current_h),
            pygame.HWSURFACE | pygame.DOUBLEBUF
        )
        pygame.display.toggle_fullscreen()
        self.screen = pygame.display.get_surface()
        self.background = Background([10000, 10000], 500, 20)
        self.gui = GUI(self.screen)

    def render(self, state: GameState):
        self.background.draw(self.screen, state.offset)

        for explosion in [(v, k) for v, k in state.physical_state.explosions.items()]:
            dob, info = explosion
            position, size = info
            age = state.physical_state.move_count - dob
            if 0 < age < 100:
                pygame.draw.circle(
                    self.screen,
                    random_orange(),
                    int_vector(add_vectors(state.offset, position)),
                    int((size * age / 30) + randint(0, 5))
                )

        for body in state.physical_state.bodies:
            sprite: Surface
            if isinstance(body, Player):
                sprite = draw_player(body)
            else:
                sprite = draw_body(body)

            sprite.set_colorkey(BLACK)
            try:
                self.screen.blit(
                    sprite,
                    subtract_vectors(
                        body.screen_position(state.offset),
                        divide_vector(sprite.get_size(), 2)
                    )
                )
            except TypeError:
                # TODO max speed
                print(body.position)
                print(body.velocity)
                exit()
        self.gui.render(state)
        pygame.display.flip()
        state.render_count += 1
