from unittest import TestCase

from main.model.bodies import Body


class BodyTest(TestCase):

    def test_step_forward_applies_velocity_to_position(self):
        body = Body(velocity=(5, 15))
        body.step_forward()
        self.assertEqual(body.position, (5, 15))

    def test_step_backward_subtracts_velocity_from_position(self):
        body = Body(velocity=(11, 1))
        body.step_backward()
        self.assertEqual(body.position, (-11, -1))

    def test_step_forward_applies_drag(self):
        body = Body(velocity=(10, 100), drag_coefficient=0.9)
        body.step_forward()
        self.assertEqual(body.velocity, (9, 90))

    def test_step_backward_unapplies_drag(self):
        body = Body(velocity=(18, -27), drag_coefficient=0.9)
        body.step_backward()
        self.assertEqual(body.velocity, (20, -30))
