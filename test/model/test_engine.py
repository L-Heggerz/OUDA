from unittest import TestCase

from main.model.bodies import Body
from main.model.engine import apply_gravity, detect_collisions, do_collisions, undo_collisions
from main.model.physical_state import PhysicalState


class EngineTest(TestCase):

    def test_bodies_are_attracted_to_each_other(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=100)
        b2 = Body(position=(100, 0), velocity=(0, 0), size=10,  density=100)
        p_state.add_body(b1)
        p_state.add_body(b2)
        apply_gravity(p_state)
        self.assertGreater(abs(b1.x_velocity), 0)
        self.assertGreater(abs(b2.x_velocity), 0)
        self.assertEqual(b1.y_velocity, 0)
        self.assertEqual(b2.y_velocity, 0)

    def test_lighter_bodies_are_attracted_more(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=100)
        b2 = Body(position=(100, 0), velocity=(0, 0), size=10,  density=1000)
        p_state.add_body(b1)
        p_state.add_body(b2)
        apply_gravity(p_state)
        self.assertGreater(abs(b1.x_velocity), abs(b2.x_velocity))

    def test_closer_bodies_are_attracted_more(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=1000)
        b2 = Body(position=(500, 0), velocity=(0, 0), size=10,  density=10)
        b3 = Body(position=(-1000, 0), velocity=(0, 0), size=10,  density=10)
        p_state.add_body(b1)
        p_state.add_body(b2)
        p_state.add_body(b3)
        apply_gravity(p_state)
        self.assertGreater(abs(b2.x_velocity), abs(b3.x_velocity))

    def test_gravity_applies_to_existing_vector(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=1000)
        b2 = Body(position=(0, 500), velocity=(0, 0), size=10,  density=10)
        b3 = Body(position=(0, -500), velocity=(0, 1), size=10,  density=10)
        p_state.add_body(b1)
        p_state.add_body(b2)
        p_state.add_body(b3)
        apply_gravity(p_state)
        self.assertLess(abs(b2.y_velocity), abs(b3.y_velocity))

    def test_apply_gravity_can_be_reversed(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=100)
        b2 = Body(position=(100, 0), velocity=(0, 0), size=10,  density=100)
        p_state.add_body(b1)
        p_state.add_body(b2)
        for i in range(0, 100):
            apply_gravity(p_state)
        for i in range(0, 100):
            apply_gravity(p_state, -1)
        self.assertAlmostEqual(b1.x_velocity, 0)
        self.assertAlmostEqual(b2.x_velocity, 0)
        self.assertAlmostEqual(b1.y_velocity, 0)
        self.assertAlmostEqual(b2.y_velocity, 0)

    def test_collisions_are_detected(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=1000)
        b2 = Body(position=(0, 500), velocity=(0, 0), size=10,  density=10)
        b3 = Body(position=(0, 10), velocity=(0, 1), size=10,  density=10)
        p_state.add_body(b1)
        p_state.add_body(b2)
        p_state.add_body(b3)
        collisions = detect_collisions(p_state)
        self.assertIn((b1, b3), collisions)
        self.assertNotIn((b2, b3), collisions)

    def test_no_collisions_are_detected(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10,  density=1000)
        b2 = Body(position=(0, 500), velocity=(0, 0), size=10,  density=10)
        b3 = Body(position=(0, 100), velocity=(0, 1), size=10,  density=10)
        p_state.add_body(b1)
        p_state.add_body(b2)
        p_state.add_body(b3)
        self.assertEqual(len(detect_collisions(p_state)), 0)

    def test_collisions_reduce_health_proportional_to_flamability(self):
        p_state = PhysicalState()
        b1 = Body(position=(0, 0), velocity=(0, 0), size=10, health=1000, flamibility=10)
        b2 = Body(position=(0, 0), velocity=(0, 0), size=10, health=1000, flamibility=1)
        p_state.add_body(b1)
        p_state.add_body(b2)
        do_collisions([[b1, b2]], p_state)
        self.assertEqual(b1.health, 990)
        self.assertEqual(b2.health, 900)

    def test_collisions_reduce_health_proportional_to_size(self):
        p_state = PhysicalState()
        b1 = Body(size=10, health=1000, flamibility=10)
        b2 = Body(size=1, health=1000, flamibility=10)
        p_state.add_body(b1)
        p_state.add_body(b2)
        do_collisions([[b1, b2]], p_state)
        self.assertEqual(b1.health, 990)
        self.assertEqual(b2.health, 900)

    def test_undo_collisions_undoes_health_damage(self):
        p_state = PhysicalState()
        b1 = Body(size=10, health=1000, flamibility=10)
        b2 = Body(size=1, health=1000, flamibility=10)
        p_state.add_body(b1)
        p_state.add_body(b2)
        do_collisions([[b1, b2]], p_state)
        undo_collisions([[b1, b2]])
        self.assertEqual(b1.health, 1000)
        self.assertEqual(b2.health, 1000)
