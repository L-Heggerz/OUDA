from random import randint
from unittest import TestCase

from main.utils.vector_utils import distance, normalize_vector, add_vectors, subtract_vectors, multiply_vector, \
    divide_vector


class VectorUtilsTest(TestCase):

    def test_normalized_vector_length_is_almost_one(self):
        for x in range(0, 100):
            vector = (randint(-1000000, 1000000), randint(-1000000, 1000000))
            self.assertAlmostEqual(distance(normalize_vector(vector)), 1)

    def test_vector_distance(self):
        self.assertAlmostEqual(distance((0, 0), (0, 5)), 5)
        self.assertAlmostEqual(distance((0, 0), (3, 4)), 5)
        self.assertAlmostEqual(distance((3, -4), (0, 0)), 5)
        self.assertAlmostEqual(distance((0, 0), (-3, -4)), 5)
        self.assertAlmostEqual(distance((0, 0), (0, 0)), 0)
        self.assertAlmostEqual(distance((0, 0), (123, 62)), 18973 ** 0.5)

    def test_vector_addition(self):
        self.assertEqual(add_vectors((0, 0), (1, 1)), (1, 1))
        self.assertEqual(add_vectors((-1, 1), (1, 1)), (0, 2))
        self.assertEqual(add_vectors((5, 9), (3, 204)), (8, 213))
        self.assertEqual(add_vectors((-1, -1), (-10, -5)), (-11, -6))

    def test_vector_subtraction(self):
        self.assertEqual(subtract_vectors((1, 1), (0, 0)), (1, 1))
        self.assertEqual(subtract_vectors((0, 0), (1, 1)), (-1, -1))
        self.assertEqual(subtract_vectors((1, 1), (3, 4)), (-2, -3))
        self.assertEqual(subtract_vectors((1000, 6900000), (420, 69)), (580, 6899931))

    def test_multiply_vector(self):
        self.assertEqual(multiply_vector((1, 1), 420), (420, 420))
        self.assertEqual(multiply_vector((1, -1), -2), (-2, 2))
        self.assertEqual(multiply_vector((3, 18), 5), (15, 90))
        self.assertEqual(multiply_vector((20, -10), -0.5), (-10, 5))

    def test_divide_vector(self):
        self.assertEqual(divide_vector((1, 1), 1), (1, 1))
        self.assertEqual(divide_vector((5, 5), 5), (1, 1))
        self.assertEqual(divide_vector((7, -7), -2), (-3.5, 3.5))
        self.assertEqual(divide_vector((-11, -19), 3), (-11 / 3, -19 / 3))
